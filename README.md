# Community Insights Email Distribution

The [Community Insights survey](https://meta.wikimedia.org/wiki/Community_Insights) has been conducted by Global Data & Insights team since 2016. The survey was developed in partnership with Foundation teams, and initially the Community Insights survey was one of the only ways to gain insights about our communities in a statistically reliable manner.  These data help paint a picture of our Movement's demographics and communities' social and technical experiences. They also tell us whether we are progressing towards the Wikimedia Foundation’s Medium Term Plan goals and the 2030 Strategic Direction. 

The sampling process for the survey can be seen [here](https://github.com/wikimedia-research/Community-Insights-sampling). Prior to 2022, the email distribution for the survey took place through the emailer service provided the survey software (Quatrics). In an attempt to increase the response rates and avoid saturating community, we are making use of MediaWiki's Emailuser feature through the API. The email will be routed through a user's homewiki, which will also trigger an on-wiki [Echo notification](https://www.mediawiki.org/wiki/Extension:Echo).

The process includes the following steps:

## Step 1: sample grouping

The Community Insights sample is usually around 25,000 registered users on Wikimedia projects. While it should be technically feasible to distribute in one batch, but from previous experience, we observed that distributing in a single batch: more prone to overall failure, slows down the script, makes handling errors hard. After various trials, splitting the sample into smaller files of 100 users each worked.

In this step, we will be taking the output from the [final sampling step](https://gitlab.wikimedia.org/repos/research/community-insights-sampling/-/blob/master/04-sampling.ipynb?ref_type=heads) and creates files for distributing the survey, which includes the following addition to the sample frame: language (for the survey to be sent in), optin link & optout link. The key to all the various files is `user_name`. The code will ensure that the all the sampled users have an assigned language, opt-in and opt-out, failing which an assertion error will be raised.

## Step 2: translations

Create a JSON with language codes and translation text (email subject, body and signature) for later use.

The JSON output should have the following structure:

<code>
{
    'en': {
        'subject': 'Hello', 
        'body': 'This is a survey'
        'signature': 'Thank you'
    },
    'fr': {
        'subject': 'Bonjour', 
        'body': 'Ceci est une enquête',
        'signature': 'merci'
    }
}
</code>

## Step 3: email distribution

The final major step is to be distribute the survey. Please complete the following setup before distribution:

* Email ca@wikimedia.org to obtain [***staff*** user-group](https://meta.wikimedia.org/wiki/Special:GlobalGroupPermissions/staff) to WMF account on Meta-Wiki. This enables `noratelimit` and `apihighlimits` flags to your account, both of which are required for the distribution.
* After obtaining the rights, go to [Special:BotPasswords on Meta-Wiki](https://meta.wikimedia.org/wiki/Special:BotPasswords), to create a bot username and password to be used for distributions. After giving the bot account a descriptive name, please enable the following grants:
    + High-volume (bot) access
    + Send email to other users
    + Create accounts
    + Access two-factor authentication (OATH) information for self and others
* The bot username and password should be stored as environment variables in `set_env_variables.py` script in the secrets folder, with variable names as `bot_username` and `bot_password`.


## Step 4 (optional): distribution status

An optional to step to check the summary of distribution status while the script is still running.


