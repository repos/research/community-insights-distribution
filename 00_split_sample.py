import os
import pandas as pd

def split_sample(sample_frame,
                 out_path,
                 batch_size=100, # recommend batch size, can be changed
                 file_prefix='group'):
    
    """
    Splits a DataFrame into multiples files (TSV format) according to the specifed batch size.
    
    Parameters
    ----------
    sample_frame (DataFrame): list of all sampled users.
    out_path (str): directory for the output TSV files.
    batch_size (int; optional): number of users per sample groupl; default to 100 - recommended.
    file prefix (str; optional): prefix to the sample group; default is 'group'.
    
    Output
    ------
    prints -> numbers of files created.
    """
    
    # ensure path exists
    assert os.path.isdir(out_path), f'output path: {out_path}, does not exist.'
    
    sample_frame = sample_frame.sort_values('home_project', ignore_index=True)
    total_rows = sample_frame.shape[0]
    file_counter = 0
    
    for start_row in range(0, total_rows, batch_size):
        file_counter += 1
        
        end_row = min(start_row + batch_size, total_rows)
        
        sample_split = sample_frame.iloc[start_row:end_row]
        
        filename = f'{out_path}{file_prefix}_{file_counter}.tsv'
        sample_split.to_csv(filename, sep='\t', index=False)
        
    print(f'# files created: {file_counter}')