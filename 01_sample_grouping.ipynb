{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "99e0e5d1-63e1-47f2-b4ea-4d29b329dd58",
   "metadata": {},
   "source": [
    "# Step 1: Sample grouping\n",
    "The Community Insights sample is usually around 25,000 registered users on Wikimedia projects. While it should be technically feasible to distribute in one batch, but from previous experience, we observed that distributing in a single batch: more prone to overall failure, slows down the script, makes handling errors hard. After various trials, splitting the sample into smaller files of 100 users each worked.\n",
    "\n",
    "**Summary**: In this step, we will be taking the output from the [final sampling step](https://gitlab.wikimedia.org/repos/research/community-insights-sampling/-/blob/master/04-sampling.ipynb?ref_type=heads) and creates files for distributing the survey, which includes the following addition to the sample frame: language (for the survey to be sent in), optin link & optout link. The key to all the various files is `user_name`. The code will ensure that the all the sampled users have an assigned language, opt-in and opt-out, failing which an assertion error will be raised."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00450042-0912-46c1-b962-69604013fd5a",
   "metadata": {},
   "source": [
    "## Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "969d176a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import json\n",
    "from functools import reduce\n",
    "import os\n",
    "import importlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "b0be145b-d786-45fa-b51a-e1be1b774da3",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# import sample split function\n",
    "\n",
    "split_sample = importlib.import_module(\"00_split_sample\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd6ce04c",
   "metadata": {},
   "source": [
    "## Preparing the sample"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12286c01-801e-458b-8118-f77409ab0ce9",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "The following files will have to be prepared:\n",
    "- sample: user_name, email, home project, global edit count, project group, global edit count bin \n",
    "    - output of [04-sampling.ipynb](https://github.com/wikimedia-research/Community-Insights-sampling/blob/master/04-sampling.ipynb)\n",
    "- language map: wiki_db, language\n",
    "- optin: user_name, optin link\n",
    "- optout: user_name, opt out link"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1541e763-f6b8-4660-b6ef-7327d26eecf8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# the paths file has pointers to the sample metadata, optin and optout links\n",
    "with open('paths.json', 'r') as file:\n",
    "    paths = json.load(file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "a718d503-2fff-4b30-8b55-bb04ccb4d16a",
   "metadata": {},
   "outputs": [],
   "source": [
    "projects = pd.read_csv('https://gitlab.wikimedia.org/repos/research/community-insights-sampling/-/raw/master/definitions/project-group-assignments.tsv', sep='\\t')\n",
    "inscope_projects = projects.project_key.values.tolist()\n",
    "\n",
    "# remove Persian projects as they will have seperate distribution mechanism, as needed.\n",
    "fa_projects = ['fawiki', 'fawikibooks', 'fawikiquote', 'fawikisource', 'fawiktionary']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d3b4adbc-dfd5-461a-9df5-1a28f01c2cb9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# remove out of scope projects (or as defined above)\n",
    "sample = (\n",
    "    pd\n",
    "    .read_csv(paths['sample'], sep='\\t')\n",
    "    .query(\"\"\"(home_project == @inscope_projects) & (home_project != @fa_projects)\"\"\")\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "2ca3f8f5-9813-423e-a433-6b27211c0b71",
   "metadata": {},
   "outputs": [],
   "source": [
    "# assign distribution language based on home wiki\n",
    "lang_map = (\n",
    "    pd\n",
    "    .read_csv(paths['lang_map'], sep='\\t')\n",
    "    .rename({\n",
    "        'wiki_db': 'home_project'\n",
    "    }, axis=1)\n",
    ")\n",
    "\n",
    "# ensure the language assignments are in lower case\n",
    "lang_map['language'] = lang_map['language'].str.lower()\n",
    "\n",
    "# replace for consistency in variable names\n",
    "lang_map = lang_map.replace({\n",
    "    'pt-br': 'ptbr',\n",
    "    'zh-s': 'zhs'\n",
    "})\n",
    "\n",
    "# ensure all the home wikis present in the sample are present in the language map\n",
    "missing_lang_map = [db for db in sample.home_project.values if db not in lang_map.home_project.values]\n",
    "assert not missing_lang_map, f'following wikis do not have a language assigned: {missing_lang_map}'\n",
    "\n",
    "# join\n",
    "sample = pd.merge(sample, lang_map, how='left', on='home_project')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "f6076358-9a19-4901-bc87-d79a9dc3509d",
   "metadata": {},
   "outputs": [],
   "source": [
    "select_cols = ['First Name', 'Link']\n",
    "\n",
    "# process optin links\n",
    "sample_optin = (\n",
    "    pd\n",
    "    .read_csv(\n",
    "        paths['optin'], \n",
    "        usecols=select_cols,\n",
    "        sep='\\t'\n",
    "    )\n",
    "    .rename({\n",
    "        'First Name': 'user_name',\n",
    "        'Link': 'in_link'\n",
    "    }, axis=1)\n",
    ")\n",
    "\n",
    "# ensure all sampled users have an opt-in link\n",
    "optin_list = [_ for _ in sample.user_name.values if _ not in sample_optin.user_name.values]\n",
    "assert not optin_list, f'following sampled users do not have an opt-in link: {optin_list}'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "0863d24f-ef5a-4d60-9899-32d4be1ae802",
   "metadata": {},
   "outputs": [],
   "source": [
    "# process optout links\n",
    "sample_optout = (\n",
    "    pd\n",
    "    .read_csv(\n",
    "        paths['optout'], \n",
    "        usecols=select_cols,\n",
    "        sep='\\t'\n",
    "    )\n",
    "    .rename({\n",
    "        'First Name': 'user_name',\n",
    "        'Link': 'out_link'\n",
    "    }, axis=1)\n",
    ")\n",
    "\n",
    "# ensure all sampled users have an opt-out link\n",
    "optout_list = [_ for _ in sample.user_name.values if _ not in sample_optout.user_name.values]\n",
    "assert not optout_list, f'following sampled users do not have an opt-out link: {optout_list}'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a24355e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# read canonical data for domain names (API endpoint)\n",
    "cd_wikis = (\n",
    "    pd\n",
    "    .read_csv(\n",
    "        paths['cd_wikis'], \n",
    "        sep='\\t', \n",
    "        usecols=['database_code', 'domain_name']\n",
    "    )\n",
    "    .rename({\n",
    "        'database_code': 'home_project'\n",
    "    }, axis=1)\n",
    ")\n",
    "\n",
    "cd_wikis.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "08929f3b-e3dc-48fc-a6b0-f38882107fa1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# merge into a single dataframe\n",
    "\n",
    "sample = reduce(\n",
    "    lambda left, right: (\n",
    "        pd.merge(\n",
    "            left, \n",
    "            right,\n",
    "            on='user_name', \n",
    "            how='inner')\n",
    "    ), [sample, sample_optin, sample_optout]\n",
    ")\n",
    "\n",
    "sample = (\n",
    "    pd.merge(\n",
    "        sample, \n",
    "        cd_wikis, \n",
    "        on='home_project', \n",
    "        how='left'\n",
    "    )\n",
    "    .replace({\n",
    "        'wikidata.org': 'www.wikidata.org'\n",
    "    })\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "06d7d31e-4729-48f6-9feb-608cda9a4d42",
   "metadata": {},
   "outputs": [],
   "source": [
    "sample.to_csv('secrets/2024/iteration_1/distribution_sample.tsv', sep='\\t', index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8249f76f",
   "metadata": {},
   "source": [
    "### Split samples"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "372ca372-ca3f-4a38-a0d9-9539ff4f782f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "# files created: 236\n"
     ]
    }
   ],
   "source": [
    "importlib.reload(split_sample)\n",
    "split_sample.split_sample(sample, 'secrets/2024/iteration_1/sample_groups/', batch_size=100)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
